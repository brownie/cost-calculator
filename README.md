COST CALCULATOR
The project source can be found here:
https://gitlab.com/ntcrotser/cost-calculator
This is an attempt to provide easy, foolproof calculations for cost vs. list price when ordering from a supplier.

TARGET PLATFORMS
*nix (x86_64) (armvl) (aarm64)

AVAILABILITY


INSTALL INSTRUCTIONS


AUTHORS
N. T. Crotser vulpisvulpis93@protonmail.ch

LICENSE
All of the files in this project are under the GLLv.3 license.  A copy of the
license is available in the LICENSE file.