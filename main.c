#include <stdio.h>
#include <stdlib.h>

int main(int args, char* argv[])
{
/*  This application is intended to make determining cost of product/supplies purchased from Canature WaterGroup/Hydrotech Water much easier and fool proof.
    In order to use this program, you'll need know what percentage off of list price you're buying at. If you don't know this please contact your sales rep.
*/

// Argument if (or case switch) structure







     printf("Welcome to the Cost Calculator!\nVersion 0.01a\n\n");
     printf("     $$    \n");
     printf("  $$$$$$$$        CCCCC   OOOOO   SSSSS   TTTTTT\n");
     printf(" $$$ $$           CC      OO OO    SS       TT\n");
     printf("  $$$$$           CC      OO OO      SS     TT\n");
     printf("   $$$$           CCCCC   OOOOO   SSSSS     TT\n");
     printf("    $$$\n");
     printf("     $$$          CCCCC    AA    LL      CCCCC\n");
     printf("     $$$$         CC     AA  AA  LL      CC\n");
     printf("     $$$$$        CC     AAAAAA  LL      CC\n");
     printf("     $$ $$$       CCCCC  AA  AA  LLLLL   CCCCC\n");
     printf("  $$$$$$$$    \n");
     printf("     $$\n\n");
     printf("This application is licensed under the Greater Lunduke License version 0.3\nThis program is provided AS-IS with NO WARRANTY\nFor more information, consult LICENSE.txt that accompanied this program\n");
     printf("Cost Calculator Â© 2019 N.T. Crotser\n\n");


     double multiply;
     double listprice;
     double cost;
     
     char sentinel = ' '; // sentinel for while loop
     
     printf("Enter your cost multiplier:   ");
     scanf("%lf", &cost);
     
     while(sentinel != 'q') // Use while loop instead of goto
     {
       printf("Enter the list price:   ");
       scanf("%lf", &listprice);
       multiply = listprice * cost;
       
       /* printf("The Item's List Price is: %f\n", listprice); */
       
       printf("*Your Cost Is: %.2f*\n\n", multiply);  // Limit trailing zeros
       
       printf("c to Continue 'q' to Quit: "); // Illusory choice, 'q' is the only recognized choice
       scanf("%c", &sentinel);
     }
}
